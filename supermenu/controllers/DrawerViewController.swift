//
//  DrawerViewController.swift
//  supermenu
//
//  Created by Владислав on 20.04.17.
//  Copyright © 2017 vladislav. All rights reserved.
//

import UIKit

class DrawerViewController: UITableViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //tableView.delegate = self
    }
}

extension DrawerViewController {
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        print("do something")
    }
}
