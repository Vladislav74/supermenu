//
//  RestaurantsListViewController.swift
//  supermenu
//
//  Created by Владислав on 26.04.17.
//  Copyright © 2017 vladislav. All rights reserved.
//

import UIKit
import CoreLocation

class RestaurantsListViewController: UIViewController {

    @IBOutlet weak var backButton : UIButton!
    @IBOutlet weak var tableView : UITableView!
    
    var location : CLLocation!
    
    var restaurants = [Restaurant]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.separatorColor = UIColor.clear
        
        let nib = UINib(nibName: RestaurantViewCell.identifier, bundle: Bundle(identifier: RestaurantViewCell.identifier))
        tableView.register(nib, forCellReuseIdentifier: RestaurantViewCell.identifier)
        
        updateData()
        
        backButton.addTarget(self, action: #selector(self.backButtonAction), for: .touchUpInside)
    }
    
    func backButtonAction() {
        navigationController?.popViewController(animated: true)
    }
    
    func updateData() {
        NetworkManager.shared.fetchRestaurantsFor(location: location) { (restaurants) in
            self.restaurants = restaurants
            self.tableView.reloadData()
        }
    }
}

extension RestaurantsListViewController : UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return restaurants.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: RestaurantViewCell.identifier, for: indexPath) as! RestaurantViewCell
        cell.configure(restaurant: restaurants[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return RestaurantViewCell.height
    }
}
