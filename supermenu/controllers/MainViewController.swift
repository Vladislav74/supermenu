//
//  MainViewController.swift
//  supermenu
//
//  Created by Владислав on 20.04.17.
//  Copyright © 2017 vladislav. All rights reserved.
//

import UIKit
import KYDrawerController
import CoreLocation

class MainViewController: UIViewController {

    @IBOutlet weak var menuButton : UIButton!
    @IBOutlet weak var headerLabel : UILabel!
    @IBOutlet weak var moneyLabel : UILabel!
    @IBOutlet weak var dishView : UIView!
    @IBOutlet weak var restaurantView : UIView!
    @IBOutlet weak var addressLabel : UILabel!
    
    @IBOutlet weak var mainImage : UIImageView!
    
    @IBOutlet weak var actionView : UIView!
    @IBOutlet weak var historyView : UIView!
    @IBOutlet weak var commentView : UIView!
    @IBOutlet weak var toInviteFriendView : UIView!
    
    @IBOutlet weak var dishButton : UIButton!
    @IBOutlet weak var restaurantButton : UIButton!
    @IBOutlet weak var actionButton : UIButton!
    @IBOutlet weak var historyButton : UIButton!
    @IBOutlet weak var commentButton : UIButton!
    @IBOutlet weak var toInviteFriendButton : UIButton!
    
    var userLocation : CLLocation?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.isHidden = true
        
        dishView.layer.cornerRadius = 6
        dishView.clipsToBounds = true
        
        restaurantView.layer.cornerRadius = 6
        restaurantView.clipsToBounds = true
        
        menuButton.addTarget(self, action: #selector(self.menuButtonAction), for: .touchUpInside)
        
        dishButton.addTarget(self, action: #selector(self.dishButtonAction), for: .touchUpInside)
        restaurantButton.addTarget(self, action: #selector(self.restaurantButtonAction), for: .touchUpInside)
        actionButton.addTarget(self, action: #selector(self.actionButtonAction), for: .touchUpInside)
        historyButton.addTarget(self, action: #selector(self.historyButtonAction), for: .touchUpInside)
        commentButton.addTarget(self, action: #selector(self.commentButtonAction), for: .touchUpInside)
        toInviteFriendButton.addTarget(self, action: #selector(self.toInviteFriendButtonAction), for: .touchUpInside)
        
        UserLocationManager.shared.fetchUserLocation { (location) in
            self.userLocation = location
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let gradientColor1 = UIColor(colorLiteralRed: 245/255, green: 245/255, blue: 245/255, alpha: 1)
        let gradientColor2 = UIColor.clear
        let gradient = CAGradientLayer()
        gradient.colors = [gradientColor2.cgColor, gradientColor1.cgColor]
        gradient.locations = [NSNumber(value: 0.4), NSNumber(value: 0.8)]
        
        gradient.frame = CGRect(x: 0, y: 0, width: mainImage.bounds.width, height: mainImage.bounds.height)
        mainImage.layer.insertSublayer(gradient, at: 0)
    }
    
    func menuButtonAction() {
        if let drawerController = navigationController?.parent as? KYDrawerController {
            drawerController.setDrawerState(.opened, animated: true)
        }
    }
    
    func dishButtonAction() {
        print("dishButtonAction")
    }
    
    func restaurantButtonAction() {
        
        if userLocation != nil {
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RestaurantsListViewController") as! RestaurantsListViewController
            vc.location = userLocation
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func actionButtonAction() {
        print("actionButtonAction")
    }
    
    func historyButtonAction() {
        print("historyButtonAction")
    }
    
    func commentButtonAction() {
        print("commentButtonAction")
    }
    
    func toInviteFriendButtonAction() {
        print("toInviteFriendButtonAction")
    }
}
