//
//  RestaurantViewCell.swift
//  supermenu
//
//  Created by Владислав on 26.04.17.
//  Copyright © 2017 vladislav. All rights reserved.
//

import UIKit
import SDWebImage

class RestaurantViewCell: UITableViewCell {

    @IBOutlet weak var restaurantImage : UIImageView!
    @IBOutlet weak var restaurantName : UILabel!
    @IBOutlet weak var timeForDeliver : UILabel!
    @IBOutlet weak var orderPrice : UILabel!
    
    static let identifier = "RestaurantViewCell"
    static let height : CGFloat = 120
    
    func configure(restaurant : Restaurant) {
        
        //restaurantImage.sd_setImage(with: URL(string: restaurant.logo))
        
        restaurantName.text = restaurant.name
        
        let deliveryTime = String(restaurant.deliveryTime) 
        let deliveryPrice = String(restaurant.deliveryPrice) 
        
        timeForDeliver.text = "Время доставки ~\(deliveryTime) мин."
        orderPrice.text = "Сумма заказа от \(deliveryPrice)Т"
    }
}
