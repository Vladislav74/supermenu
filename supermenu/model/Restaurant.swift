//
//  Restaurant.swift
//  supermenu
//
//  Created by Владислав on 19.04.17.
//  Copyright © 2017 vladislav. All rights reserved.
//
import SwiftyJSON

class RestaurantLogo {
    
    let id : Int!
    let path : String!
    let fileName : String!
    let hasPreview : Bool!
    
    init(jsonData : [String : AnyObject]) {
        
        let json = JSON(jsonData)
        
        self.id = json["id"].int ?? 0
        self.path = json["path"].string ?? ""
        self.fileName = json["fileName"].string ?? ""
        self.hasPreview = json["hasPreview"].bool ?? false
    }
    
}

class WorkingHours {
    
    let id : Int!
    let startTime : String!
    let endTime : String!
    
    init(jsonData : JSON) {
        self.id = jsonData["id"].int ?? 0
        self.startTime = jsonData["startTime"].string ?? ""
        self.endTime = jsonData["endTime"].string ?? ""
    }
}

class RestaurantWorkingDay {
    
    let id : Int!
    var workingHoursList = [WorkingHours]()
    let dayOfWeek : String!
    
    init(jsonData : JSON) {
        
        self.id = jsonData["id"].int ?? 0
        self.dayOfWeek = jsonData["dayOfWeek"].string ?? ""
        
        if let workingHoursJsonArray = jsonData["workingHoursList"].array {
            for workingHoursJson in workingHoursJsonArray {
                workingHoursList.append(WorkingHours(jsonData : workingHoursJson))
            }
        }
    }
}

class RestaurantBranches {
    
    let id : Int!
    var workingDays = [RestaurantWorkingDay]()
    let address : String!
    let latitude : Float!
    let longitude : Float!
    //let contacts : []
    
    init(jsonDataArray : [JSON]) {
        
        let jsonData = jsonDataArray[0]
        
        self.id = jsonData["id"].int ?? 0
        self.address = jsonData["paaddressth"].string ?? ""
        self.latitude = jsonData["longitude"].float ?? 0
        self.longitude = jsonData["longitude"].float ?? 0
        
        if let workingDaysJsonArray = jsonData["workingDays"].array {
            for workingDaysJson in workingDaysJsonArray {
                workingDays.append(RestaurantWorkingDay(jsonData : workingDaysJson))
            }
        }
    }
}

class Restaurant {
    
    let id : Int!
    let name : String!
    let logo : RestaurantLogo!
    let restaurantBranches : RestaurantBranches!
    let deliveryTime : Int!
    let orderMinPrice : Int!
    let deliveryPrice : Int!
    let reviewsCount : Int!
    let rating : Float!
    let hasPromo : Bool!
    var paymentTypes = [String]()
    let closed : Bool!
    
    init(jsonData : [String : AnyObject]) {
        
        let json = JSON(jsonData)
        
        self.id = json["id"].int ?? 0
        self.name = json["name"].string ?? ""
        
        self.logo = RestaurantLogo(jsonData: json["logo"].dictionaryObject! as [String : AnyObject])
        self.restaurantBranches = RestaurantBranches(jsonDataArray: json["restaurantBranches"].array!)
        
        self.deliveryTime = json["deliveryTime"].int ?? 0
        self.orderMinPrice = json["orderMinPrice"].int ?? 0
        self.deliveryPrice = json["deliveryPrice"].int ?? 0
        self.reviewsCount = json["reviewsCount"].int ?? 0
        self.rating = json["rating"].float ?? 0
        self.hasPromo = json["hasPromo"].bool ?? false
        
        if let payment = json["paymentTypes"].arrayObject as? [String] {
            self.paymentTypes = payment
        }
        
        self.closed = json["closed"].bool ?? false
        
        print(name)
    }
}
