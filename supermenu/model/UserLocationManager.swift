//
//  UserLocationManager.swift
//  supermenu
//
//  Created by Владислав on 26.04.17.
//  Copyright © 2017 vladislav. All rights reserved.
//

import Foundation
import CoreLocation

class UserLocationManager {
    
    static let shared = UserLocationManager()
    
    func fetchUserLocation(completion : (_ location : CLLocation) -> Void) {
        
        let location = CLLocation(latitude: 73.092423, longitude: 49.802033)
        
        completion(location)
    }
}
