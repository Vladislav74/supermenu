//
//  NetworkManager.swift
//  supermenu
//
//  Created by Владислав on 19.04.17.
//  Copyright © 2017 vladislav. All rights reserved.
//
import Alamofire
import CoreLocation
import SwiftyJSON

class NetworkManager {
    
    static let shared = NetworkManager()
    
    private let baseUrl = "https://api-dev.supermenu.kz/restaurants?"
    
    func fetchRestaurantsFor(location: CLLocation, completion: @escaping (_ restaurants : [Restaurant]) -> Void) {
        
        let longitude = 73.092423 // location.coordinate.longitude
        let latitude = 49.802033 // location.coordinate.latitude
        
        let urlString = baseUrl + "longitude=\(longitude)&latitude=\(latitude)"
     
        print(urlString)
        
        if let url = URL(string : urlString) {
            Alamofire.request(url).responseJSON { response in
                
                //print(response.value ?? "")
                
                var restaurants = [Restaurant]()
                
                guard response.result.isSuccess else {
                    return
                }
                if let response = response.result.value as? [String : AnyObject] {
                    
                    if let restaurantsJsonArray = response["data"] as? [[String : AnyObject]] {
                        
                        for restaurantsJson in restaurantsJsonArray {
                            restaurants.append(Restaurant(jsonData : restaurantsJson))
                        }
                        completion(restaurants)
                    }
                }
            }
 
        }
    }
}
